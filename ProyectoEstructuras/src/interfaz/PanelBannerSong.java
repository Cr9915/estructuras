package interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelBannerSong extends JPanel
{
	
	// -----------------------------------------------------
	// Atributos
	// -----------------------------------------------------
		
	/** Etiqueta para mostrar la imagen del banner.	 */
		private JLabel labImage;
		
	// -----------------------------------------------------
	// Constructor
	// -----------------------------------------------------
		
    /** Crea el panel del banner. */
		
	public PanelBannerSong( )
	{
		// Se crea el objeto del panel
		labImage = new JLabel(new ImageIcon("data/guitarra.jpg") );
			
		// Agregar el objeto al panel
		add( labImage );
	}

}
